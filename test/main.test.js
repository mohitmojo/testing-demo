const request = require("supertest");
const app = require("../app");

describe("home page", function() {
  it("welcomes the user", function(done) {
    request(app)
      .get("/")
      .expect(200)
      .expect(/Welcome, Mohit!/, done);
  });
});

describe("contact form", function() {
  it("thanks the user after submitting the form", function(done) {
    request(app)
      .post("/contact")
      .send({ name: "Mohit" })
      .expect(302)
      .expect("Location", /\/thank-you/, function() {
        request(app)
          .get("/thank-you")
          .expect(200)
          .expect(/Form Submitted/, done);
      });
  });
});

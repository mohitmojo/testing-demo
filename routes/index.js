var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", { title: "Testing demo", user: "Mohit" });
});

/* GET thank-you page. */
router.get("/thank-you", function(req, res, next) {
  res.render("thank-you", {
    heading: "Form Submitted",
    title: "Thanks for submitting the form"
  });
});

// redirect to thank-you route after submitting the form
router.post("/contact", (req, res, next) => {
  res.redirect("/thank-you");
});

module.exports = router;
